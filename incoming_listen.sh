#!/bin/bash

# Start/stop a screen session with the mqtt listener:

# Change to directory w/ script:
cd "$(dirname "${BASH_SOURCE[0]}")"

# Collect CentOS cert locations and any other needed variables:
source lib/mqtt_setup.sh



# Launch a screen session labelled "RockyIncoming" and connect to 
if [[ "$1" == "start" ]]; then
  
  # First make sure we aren't already running:
  screen -ls | grep -q "RockyIncoming"

  if [[ "$?" == "0" ]]; then
    echo "ERROR: Already detected a screen session running labelled RockyIncoming.  Please stop old processes before launching"
    exit 1
  fi


  # Start up process in a screen session, writing output to mqtt_data/mqtt_output.txt
  mkdir -p mqtt_data
  screen -S RockyIncoming -d -m bash -c "mosquitto_sub ${CENTOS_GIT_CREDS} -h mqtt.git.centos.org -p 8883 -t \"git.centos.org/#\" --keepalive 60  2>&1 | tee -a mqtt_data/mqtt_output.txt" 
  exit 0
fi



# If stopping, send a ctrl+c signal to the RockyIncoming screen session:
if [[ "$1" == "stop" ]]; then
  echo "Stopping RockyIncoming / mosquitto_sub process...."
  
  screen -S RockyIncoming -X stuff "^c"
  
  sleep 1
  exit 0

fi



# Error out if we don't have start or stop as an argument:
echo "ERROR - Must invoke this script with start or stop:  $(basename "$0") start   OR    $(basename "$0") stop"
echo "This will start or stop the git.centos.org MQTT / Mosquitto listener in a screen session"
exit 1
