# Rocky INCOMING(!)
###################


## Introduction (What is it?)

Rocky Incoming is a tracker and presenter of incoming RHEL updates.  When a commit/push is made to git.centos.org (host of the RHEL sources), a message in JSON format is published to a queue (MQTT protocol).  The message contains information about the branch, tag, etc. that was committed for RHEL.  

This project collects a complete log of that queue, parses it, and checks whether the same commits/package versions have landed in Rocky Linux Git, and as RPMs in the Rocky Linux repositories.  It turns this information into an easily readable CSV file, and then displayed in a presentable form via HTML/CSS/Javascript page (JQuery).


The idea is to monitor all incoming RHEL updates, and make sure that Rocky Linux collects and builds them all to match.


<br />

## Installation

There are a few different pieces to this project, and each has to be configured.


### Requirements:

**Programs/Software Needed: ** ```bash , git , python3 , dnf , mosquitto_sub , GNU screen , web server(static content)```

On a Rocky Linux 8 (or other EL8) system:
```
dnf install epel-release

dnf install bash git python3 mosquitto screen
```

**Certificates/keys: ** A git.centos.org subscription with CA, certificate, and key is required to access the MQTT feed.

**DNF :** The Rocky source RPM repos are required to be configured in DNF (baseos-source, appstream-source, powertools-source)

<br />

### Setup:

1. Clone this project into a folder of your choice (default: **/opt/rockyincoming**)
2. Put your git.centos.org certificate, key, and CA files somewhere safe on the system (default location: **$HOME/.centos-server-ca.cert , $HOME/.centos.cert**)
3. Copy or symlink **RockyIncoming_cron** file into **/etc/cron.d/** 
4. Edit **RockyIncoming_cron** file:  change PROJECT_PATH to match the location of the **rockyincoming/** folder.  Change USER in all cron lines to be the user that owns the rockyincoming/ folder.
5. Edit **lib/setup.py** : Ensure Git sites are correct, and that the DNF repo names are correct (the repositories DO NOT need to be enabled, but they must exist!  Make sure they are the Rocky source (SRPM) repositories!)
6. Edit **lib/mqtt_setup.sh** : make --cafile  , --cert , and --key point to the correct files for accessing the queue feed.

<br />

## Launching

Once setup is complete, start the incoming_listen.sh service:

```
incoming_listen.sh start
```

This should launch a detached screen session with mosquitto_sub listening for incoming JSON from the Red Hat feed.  It collects output in **mqtt_data/mqtt_output.txt**.

If the cron job has been successfully installed, this .txt file will be rotated to a date-stamped file every hour, and the original wiped clean (ready for more output from the listener).

The cron job will also launch **incoming_get.py**, which should take all output from the mqtt_data/ folder and output an HTML file to standard out.  The HTML file depends on JQuery and Datatables Javascript libraries, which are provided in the **html/** directory of this project.  The output of the Python script should be placed in a file (like index.html) inside that html/ folder.

The contents of the **html/** folder can be copied or symbolic-linked into a folder served by a web server, so others can access the content.


Alternately, **incoming_get.py** can produce a CSV file if you wish, with pipe ( | ) separators.  Simply use the **--csv** option in the arguments and it will activate CSV mode.



Old mqtt data in the mqtt_data/ folder gets automatically removed after 45 days.  It's assumed by then that the import isn't relevant anymore.  This behavior can be tweaked by editing the cron job.

