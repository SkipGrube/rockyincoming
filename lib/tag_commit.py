# Simple object to hold data from a tagged commit
# including: tag, import_date, branch, Git URL, and project name (SRPM name)

import subprocess, re

import lib.setup as setup

class tag_commit:
  
  # Timestamp of RHEL Git update
  commit_time = ""
  
  # Timestamp of Rocky build (if applicable) 
  rocky_build_time = ""

  # The Git project's name (bash, httpd, etc.)
  repo_name = ""
    
  # Upstream branch from git.centos.org
  original_branch = ""
  
  # Matching rocky branch
  rocky_branch = ""

  # name and version of the incoming tag
  tagged_version = ""

  # Full Git tag from Rocky and upstream
  original_tag = ""
  rocky_tag = ""

  # Have we imported into Rocky Git?  Have we published to the Rocky Yum repos yet?
  rocky_imported = "No"
  rocky_built = "No"
  
  # Sanitized package name-version-release:
  pkg_version = ""
  
  
  def __init__(self, commit_time, json_data, rockyRpmSource):
    
    self.commit_time = commit_time
    self.rocky_build_time = "N/A"
    self.repo_name = json_data['repo']['name']
    self.namespace = json_data['repo']['namespace']

    self.original_tag = json_data['tag']
    
    # Rocky tag should simply be the "c8" branch replaced with an "r8"
    self.rocky_tag = self.original_tag.replace('/c8', '/r8', 1)
    
    # derive branch from tag - the 2nd to last item in the split tag string
    # ex:  tag imports/c8/libffi-3.1-23.el8  ---->  "c8" is the branch name
    self.original_branch = self.original_tag.split("/")[len(self.original_tag.split("/")) - 2]
    self.rocky_branch = self.rocky_tag.split("/")[len(self.rocky_tag.split("/")) - 2]
    
    # Get the last part of the tag, should be only the project+version tag:
    self.tagged_version = self.original_tag.split("/")[len(self.original_tag.split("/")) - 1]

    self.rocky_imported = self.checkRockyImport(self.tagged_version, self.repo_name, self.namespace)
    
    # Sanitize the tagged version to match packages:  We need to remove the "+el8+###+#######" from module packages, 
    # and we need to remove the .el8_# tag from normal packages, because these from RHEL upstream may not match 
    self.pkg_version = re.sub('(.+\.module)\+(el\d+\.\d+\.\d+)\+(\d+)\+([A-Za-z0-9]{8})(\..+)?', '\\1\\5', self.tagged_version)
    self.pkg_version = re.sub('(\.el\d+(?:_\d+|))', '', self.pkg_version)
    

    # Check whether our package is built and in the Rocky repos
    # (We need to cover the 3 possibilities: built and same version, not the same version, or Rocky doesn't have the RPM at all)
    self.rocky_built = "No"
    self.rocky_build_time = "N/A"
    
    
    # If the package name is in our source RPM package dictionary, we want to iterate over it, and see if we can find a matching version
    # We also want to keep track of each iteration, so we can match the build time with the corresponding __buildtime dictionary entry.

    # Example, we have rockyRpmSource['bash'] == "bash-5.2.35#bash-5.2.36#" , and the corresponding rockyRpmSource['bash__buildtime'] = "2021-07-08 11:55#2021-12-10 20:15#"
    # We split each of the strings over "#", and search for the version.  If we find a match, we use that "index" of the string and the __buildtime.  If we're looking for bash-5.2.36, we'd also get the
    # 2nd __buildtime, 2021-12-10 20:15
    if self.repo_name in rockyRpmSource:
      possiblePkgs = rockyRpmSource[self.repo_name].split('#')
      possibleVersions = rockyRpmSource[self.repo_name + "__buildtime"].split('#')
      cnt = 0
      for p in possiblePkgs:
        if p == self.pkg_version:
          self.rocky_built = "Yes"
          self.rocky_build_time = str(possibleVersions[cnt])
        cnt += 1
    
    if self.repo_name not in rockyRpmSource:
      self.rocky_built = "No (Not Present)"
          
  
  # Debug: print out variables from object
  def debugMe(self):
    print("repo_name == " + str(self.repo_name))
    print("namespace == " + str(self.namespace))
    print("commit_time == " + str(self.commit_time))
    print("original_tag == " + str(self.original_tag))
    print("rocky_tag == " + str(self.rocky_tag))
    print("original_branch == " + str(self.original_branch))
    print("rocky_branch == " + str(self.rocky_branch))
    print("tagged_version == " + str(self.tagged_version))
    print("\n\n\n")


  # Output ourself in csv mode:
  def printCommitCsv(self):
    print(str(self.commit_time) + " | " + str(self.repo_name) + "  |  " + str(self.tagged_version) + "  |  " + str(self.original_branch) + "  |  " + str(self.rocky_imported) + "  |  " + str(self.rocky_built) + "  |  " + str(self.rocky_build_time))
    
  
  # Return an html table row representation of the data:
  def getCommitHtml(self):
    return "<tr><td>" + str(self.commit_time) + "</td> <td> " + str(self.repo_name) + "</td> <td>" + str(self.tagged_version) + "</td> <td>" + str(self.original_branch) + "</td> <td>" + str(self.rocky_imported) + "</td> <td>" + str(self.rocky_built) + "</td>  <td>" + str(self.rocky_build_time) + "</td></tr>\n"
    

  def checkRockyImport(self, tag, name, namespace):
  
    # This is ugly - might should use the actual Python git bindings
    # Do a lightweight ls-remote --tags git call to the Rocky Gitlab.  Then we can see if the tag is in the returned list
    git_result = subprocess.check_output(['git', 'ls-remote', '--tags', setup.ROCKY_PREFIX + str(namespace) + "/" + str(name) + ".git", self.rocky_branch + "/" + tag])
        
    if tag in str(git_result):
      #print("found tag in Rocky!")
      return "Yes"
    else:
      #print("Tag missing!")
      return "No"
    
    
    
    
    
    
