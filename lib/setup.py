# global variables that the git_monitor program uses:


# Prefixes are the URL before "rpms/RPMNAME"
ROCKY_PREFIX = "https://git.rockylinux.org/staging/"

UPSTREAM_PREFIX = "https://git.centos.org/"

# Comma-separated list (no spaces) of Rocky DNF repos to query against
# (to match RHEL import tags <----> Rocky packages
DNF_REPOS="baseos-source,appstream-source,powertools-source,extras-source,plus-source,ha-source,resilient-storage-source,realtime-source"


# List of allowed branch patterns - these are strings which, if found in a branchname, we want to process that commit!
# Remember that if *any* part of this string appears in a branch, then it will be detected!
# Example:  '/c8'  will count for branch '/c8', but will also count for branch 'c8-stream-2.5'
allowed_branches = ['/c8', '/c8-']


# List of DISallowed branch patterns - if we find one of these in a branch name, we WILL NOT process that commit!
# Disallowed branch patterns will ALWAYS TRUMP the allowed ones.
# Example:  the branch for a commit is '/c8s-stream-2.5'.  It contains the allowed pattern "/c8", but contains the disallowed pattern "/c8s" .
# Disallowed always wins, and this commit will not be processed.
disallowed_branches = ['/c8s', '/c8-beta', '/c8-container']
