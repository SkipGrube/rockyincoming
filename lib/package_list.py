# Read all packages in the repos specified, and build a dictionary of pkg['name'] : "name-version-release"
#
# The objective is to be able to compare these versions from the Rocky repository against the incoming tags/releases from git.centos.org

import subprocess, re, sys, shutil, os

import lib.setup as setup

class package_list:

  # Dictionary that holds package name and "sanitized" release tag (module and el# info removed)
  sourcePkg = {}
  
  def __init__(self):
    self.sourcePkg = {}
    
    # Run command, get output, and translate into string:
    if os.path.exists('/tmp/dnf_incoming_cache'):
      shutil.rmtree('/tmp/dnf_incoming_cache')
    
    dnf_cmd = subprocess.Popen('dnf --setopt=cachedir=/tmp/dnf_incoming_cache  repoquery --repo "' + str(setup.DNF_REPOS) + '"   --queryformat  "%{name}@%{name}-%{version}-%{release}"@%{buildtime}', shell=True, stdout=subprocess.PIPE)
    pkg_result = dnf_cmd.stdout.read().decode('utf-8').split("\n")


    # Import each line of the repoquery output into our dictionary:  sourcePkg['package_name'] = 'name-version-release'
    for pkg in pkg_result:
      
      # Take the package version and remove the "+###+###+###" tag from modular packages, and the ".el#" tag from normal packages
      # We do the same for the RHEL Git tags, so the version comparison is valid!      
      pkg = re.sub('(.+\.module)\+(el\d+\.\d+\.\d+)\+(\d+)\+([A-Za-z0-9]{8})(\..+)?', '\\1\\5', pkg)
      pkg = re.sub('(\.el\d+(?:_\d+|))', '', pkg)
      
      
      # Actually populate the dictionary, with ['name'] == "name-version-release" (from our dnf repoquery output)
      if "@" in pkg: 
        p_line = pkg.split("@")
        
        # If our dictionary doesn't have an entry yet, then create one as blank string
        # (for both sourcePkg[pkgName] and sourcePkg[pkgName__buildtime])
        if str(p_line[0]) not in self.sourcePkg:
          self.sourcePkg[str(p_line[0])] = ''
          self.sourcePkg[str(p_line[0]) + "__buildtime"] = ''
        
        # We want to handle duplicate packages (multiple versions), so those are separated with a '#'.
        # So we might have: 
        #   sourcePkg['bash'] = "bash-5.2.35#bash-5.2.36#"
        #   sourcePkg['bash__buildtime'] = "2021-07-08 11:55#2021-12-10 20:15#"
        self.sourcePkg[str(p_line[0])] += str(p_line[1]) + '#'
        self.sourcePkg[str(p_line[0]) + "__buildtime"] += str(p_line[2]) + '#'

